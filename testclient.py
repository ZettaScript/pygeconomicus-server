from utils import *

PROTOCOL_VERSION = b"geco0"
PROTOCOL_LANG = b"json"
RECBUF = 1024
ADDR = socket.gethostname()

# Default game constants
CONSTS = {
	"N_VALUES": 4, # number of values types
	"N_WVALUES": 1, # number of waiting values types
	"TURN_DURATION": 300, # in seconds
	"N_TURNS": 10, # number of turns in a game
	"TURN_YEARS": 8, # number of years by turn
	"LIFESPAN": 10, # max age
	"N_DIGITS": 0, # number of decimals
	"START_VALUES": 4 # number of random values for beginning
}

# Test game constants (just for testing, do not play with that values)
CONSTS2 = {
	"N_VALUES": 5, # number of values types
	"N_WVALUES": 1, # number of waiting values types
	"TURN_DURATION": 600, # in seconds
	"N_TURNS": 12, # number of turns in a game
	"TURN_YEARS": 7, # number of years by turn
	"LIFESPAN": 9, # max age
	"N_DIGITS": 2, # number of decimals
	"START_VALUES": 5 # number of random values for beginning
}

HOST = getargv(sys.argv, "-h", "127.0.0.1")
try:
	PORT = int(getargv(sys.argv, "-p", "8651"))
except ValueError:
	print("Error: port must be an integer")
	exit(1)

if "--help" in sys.argv:
	print("\n\
PyĞeconomicus test client\n\
\n\
Options:\n\
  -h <hostname>  Server hostname (default: '"+HOST+"')\n\
  -p <port>      Server port (default: "+str(PORT)+")\n\
  --help         Show this message\n\
")
	exit()

# New game settings
NEWGAME_DEFS = {
	1:{},# Empty, should return error 49
	2:{"name":"Test game"},# Should work once with default values, and return 48 after
	3:{"name":"Foobar", "admin_name":"John Doe", "admin_address":"testaddr", "admin_port":1234, "admin_psw":"admin", "public":False, "consts":CONSTS2}# Should set every parameter
}

server_address = (HOST, PORT)
print("Server: "+str(server_address))
print("Type 'h()' for help, 'exit()' or CTRL+C to quit.")

def sdata(data, response_intended, lng="json"):
	"""Send encoded data"""
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect(server_address)
	
	raw = json.dumps(data).encode()
	msg = PROTOCOL_VERSION+b"/"+PROTOCOL_LANG+b"/"+hex(len(raw))[2:].encode()+b"\n"+raw
	sock.sendall(msg)
	
	resp = None
	if response_intended:
		paquet = b""
		p_len_tmp = ""
		p_len = None
		p_data_index = None
		i = 0
		while True:
			try:
				raw = sock.recv(RECBUF)
			except socket.timeout:
				break
			if raw:
				for c in raw:
					if p_len == None:
						if c == 10:# LF
							p_len = int(p_len_tmp, 16)
							p_data_index = len(paquet)+1
						elif (c >= 48 and c <= 57) or (c >= 97 and c <= 102) or (c >= 65 and c <= 70):# [0-9a-fA-F]
							p_len_tmp += chr(c)
						else:
							p_len_tmp = ""
					else:
						i += 1
					paquet += bytes([c])
				if p_len != None and i >= p_len:
					break
			else:
				break
		sock.close()
		
		# Parse paquet
		try:
			seps = [paquet.index(b"/")]
			p_ver = paquet[:seps[0]]# protocol version
			if p_ver != PROTOCOL_VERSION:
				print("Communication error: unsupported protocol version");
				return None
			seps.append(paquet.index(b"/", seps[0]+1))
			seps.append(paquet.index(b"\n", seps[1]+1))
		except ValueError:
			print("Communication error: decoding header");
			return None
		
		p_lng = paquet[seps[0]+1:seps[1]]# data language
		
		if p_len > 0:
			p_raw = paquet[p_data_index:]
			
			if p_lng == b"json":
				try:
					p_data = json.loads(p_raw)
				except json.JSONDecodeError:
					print("Communication error: decoding JSON");
					return None
				resp = str(p_data)
			
			elif p_lng == b"raw":
				resp = p_raw
	
	else:
		sock.close()
	return resp

def info():
	"""Query server info"""
	return sdata({"query":["server_info"]}, True)

def games():
	"""Query games list"""
	return sdata({"query":["games"]}, True)

def game(name="Test game"):
	"""Query game info"""
	return sdata({"game_name":name, "query":["game_info"]}, True)

def newgamesets(sets):
	"""New game from predefined settings"""
	return sdata({"new_game":NEWGAME_DEFS[sets]}, True)

def newgame(name="Test game", admin_name="Guru", admin_address=ADDR, admin_port=8654, admin_psw="", public=True, consts=CONSTS):
	"""New game"""
	return sdata({"new_game":{"name":name, "admin_name":admin_name, "admin_address":admin_address, "admin_port":admin_port, "admin_psw":admin_psw, "public":public, "consts":consts}}, True)

def editgame(name, admin_psw, sets):
	"""Edit game"""
	return sdata({"game_name":name, "admin_psw":admin_psw, "edit_game":sets}, True)

def removegame(name, admin_psw):
	"""Remove game"""
	return sdata({"game_name":name, "admin_psw":admin_psw, "rm_game":True}, True)

def newplayer(name="Test game", player_name="Little Beetle", player_address=ADDR, player_port=8654, player_psw=""):
	"""New player"""
	return sdata({"game_name":name, "new_player":{"player_name":player_name, "player_address":player_address, "player_port":player_port, "player_psw":player_psw}}, True)

def removeplayer(name="Test game", player_uid=1, player_psw="", admin_psw=None):
	"""Remove player"""
	if admin_psw != None:
		return sdata({"game_name":name, "rm_player":{"player_uid":player_uid, "admin_psw":admin_psw, "player_psw":player_psw}}, True)
	else:
		return sdata({"game_name":name, "rm_player":{"player_uid":player_uid, "player_psw":player_psw}}, True)

def exchanges(data, name="Test game", player_uid=1, player_psw=""):
	"""Send exchanges"""
	return sdata({"game_name":name, "exchanges":{"player_uid":player_uid, "player_psw":player_psw, "exchanges_data":data}}, True)

def getsource(outfile="serversourcecode.tar.gz"):
	"""Request for source code"""
	f = open(outfile, "wb")
	f.write(sdata({"source":True}, True))
	f.close()
	return outfile

def h():
	"""Show help"""
	help("__main__")

while True:
	try:
		print(eval(input("g>")))
	except KeyboardInterrupt:
		print()
		exit()
