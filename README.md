Ce programme est une implémentation du jeu Ğeconomicus pour jouer par Internet en client-serveur (et éventuellement un peu en P2P).
Ceci est la partie serveur, en python3.

Un logiciel client sera implémenté aussi, probablement en JavaScript pour être plus facile d'utilisation (pas d'installation pour le joueur).
Le serveur permettra d'héberger plusieurs parties, afin d'épargner aussi aux animateurs l'installation. Les parties pourront être crées à distance, depuis le client.
(Si vous voulez m'aider pour la réalisation du client, ça serait avec grand plaisir !)

N'hésitez pas à me contacter si vous avez des idées ou des critiques concernant le principe ou ma manière d'implémenter, de coder... Il faut adapter les règles du jeu au support informatique, ce qui nécessite de faire des choix (par exemple, j'ai décidé de remplacer le système de billets par une valeur numérique, mais je garde le roulement des valeurs).

## Utilisation

Le seul paquet **python3** est nécessaire. Lancez le serveur et le client de test comme suit&nbsp;:

	python3 server.py -h 127.0.0.1
	python3 testclient.py -h 127.0.0.1

## Documentation

La classe `Game` représente une partie, et peut servir pour toute la durée d'un jeu, pour plusieurs systèmes monétaires.
La partie serveur, pour les interactions, ainsi que le système d'échanges ne sont pas encore implémentés. La classe de la monnaie dette non plus.
Le système de roulement des valeurs est aussi à faire.

Exemple :

	import server
	
	# Initialiser le jeu
	game = server.Game("Partie de test", "Maître du jeu", ("127.0.0.1",8652), server.CONSTS.copy())
	
	# Ajoutez autant de joueurs que vous voulez...
	game.addPlayer("Alice", ("127.0.0.1",8653))
	game.addPlayer("Bob", ("127.0.0.1",8654))
	game.addPlayer("Eve", ("127.0.0.1",8655))
	
	# Démarrer une partie en monnaie libre
	game.startGame(server.money_free)
	
	game.newTurn()# Commencer le premier tour
	game.endTurn()# Terminer le premier tour
	game.newTurn()# Commencer le deuxième tour
	game.endTurn()# Terminer le deuxième tour
	
	print(game.players[0].wallet)# Les joueurs ont créé leur DU
	print(server.moneySupply(game.players))# On peut voir la masse monétaire doubler de tour en tour

Le fichier `testclient.py` est un client temporaire pour tester le serveur en développement.

### Protocole

Le protocole utilisé entre clients et serveur est très simple.

	VERSION/LANG/LENGTH\nDATA

**VERSION** étant la version du protocole utilisée pour le paquet, **LANG** le langage utilisé pour coder les données, **LENGTH** la taille des données, **DATA** les données.  
Les champs de l'entête sont séparés par un slash, l'entête est suivie d'une nouvelle ligne puis des données. Pour le moment, seul le JSON est utilisé.

Les données JSON comprennent un champ "error".

	0: OK
	16: Communication error: decoding header
	17: Communication error: unsupported protocol version
	18: Communication error: unknown language
	19: Communication error: decoding JSON
	48: Game error: that game name is already used
	49: Game error: miss new game name
	50: Game error: miss game name
	51: Game error: there is no game with this name
	52: Game error: miss admin password
	53: Game error: wrong admin password
	54: Game error: that player does not exist
	55: Game error: wrong player password
	56: Game error: miss player password
	57: Game error: miss exchanges data
	58: Game error: invalid exchange data
	128: Server error: source not available

Exemple de paquet valide :
	geco42/json/109
	{"error": 0, "server_info": {"version": "1.2.3", "protocol_versions": ["geco40","geco41","geco42","geco43"]}}

#### Échanges

Un échange de valeurs et/ou d'argent entre joueurs nécessite l'accord de tous les joueurs devant fournir de la richesse.  
Pour proposer un échange, les joueurs négocient entre eux (en P2P) les conditions. Quand ils sont en accord, ils envoient chacun au serveur les données de l'échange, contenant la liste des transactions. Le serveur valide les transactions dès qu'il a reçu la confirmation de tous les joueurs impliqués (devant donner quelque chose).

	{ "exchanges": {
		"player_uid": player_uid,
		"player_psw": player_psw,
		"exchanges_data": [
			[
				[from_player_uid, to_player_uid, amount],
				...
			],
			...
		]
	}
	# (int) amount: monnaie
	# (iterable) amount: valeurs (liste de taille N_VALUES)
	# "exchanges_data": liste des échanges.
	# Un échange est une liste de transactions d'un joueur vers un autre.

## Sources

http://www.trm.creationmonetaire.info/TheorieRelativedelaMonnaie.pdf  
http://geconomicus.glibre.org/rules.html  
https://www.youtube.com/watch?v=GMvQxk6mXhU  
https://www.le-sou.org/2018/01/08/retour-experience-animation-jeu-geconomicus/

## Licence

Copyright 2018 Pascal Engélibert

> This file is part of Pygeconomicus-server.
> 
> Pygeconomicus-server is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> Pygeconomicus-server is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
> 
> You should have received a copy of the GNU Affero General Public License
> along with Pygeconomicus-server.  If not, see <https://www.gnu.org/licenses/>.

La GNU AGPL impose de publier les sources d'une instance publiquement accessible de ce logiciel, si celles-ci sont modifiées. Pour faciliter cela, le serveur peut générer et fournir une archive de son propre code source au client si celui-ci en fait la requête.  
Si vous hébergez une instance modifiée de ce logiciel accessible publiquement, vous devez juste vérifier que tous les fichiers du logiciel soient également accessibles, donc si vous ajoutez des fichiers il faut actualiser la liste `SOURCE_FILES` dans `utils.py`. Cela est aussi valable pour les bibliothèques utilisées, qui doivent être libres. En cas de doute, vous pouvez chercher dans la FAQ de GNU ou me demander.

## Contact

[Contactez-moi via le site de ZettaScript](https://zettascript.org/contact.php)
[Page du projet](https://zettascript.org/projects/geconomicus/)
[Forum Duniter](https://forum.duniter.org/)
[Dépôt GitLab Framagit](https://framagit.org/ZettaScript/pygeconomicus-server) &#8211; [Dépôt GitLab Duniter](https://git.duniter.org/tuxmain/pygeconomicus-server)
[Clé publique Ğ1 45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ](https://g1.duniter.fr#/app/wot/45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ/tuxmain)
