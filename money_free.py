#!/usr/bin/env python3

"""
	This file is part of Pygeconomicus-server.

	Pygeconomicus-server is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Pygeconomicus-server is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pygeconomicus-server.  If not, see <https://www.gnu.org/licenses/>.
"""

from utils import *

START_WALLET = 7
UDV = 1.0905 # UD Variation 109.05%/year is ~200%/8 years

# Free money
class MoneySystem():
	name = "free_money"
	
	def __init__(self, players, consts):
		self.players = players
		self.C = consts
		self.UD = START_WALLET
		self.UDV = UDV
	
	# Init one player
	def initPlayer(self, player, reborn=False):
		if not reborn:
			player.wallet = START_WALLET
	
	# Init game part
	def startGame(self):
		# Init players
		for player in self.players:
			self.initPlayer(player, False)
	
	# New turn
	def newTurn(self, turn):
		if turn > 1:
			# Create UD
			M = moneySupply(self.players)
			N = len(self.players)
			# M[n] = M[n-1] + MAX( UD[n-1] ; M[n-1]*V ) with M=money supply, V=variation
			# M[n] = M[n-y] + MAX( UD[n-y] ; M[n-y]*V^y ) with y=years since the last UD
			self.UD = round(max(self.UD, (M*self.UDV**self.C["TURN_YEARS"]-M)/N), self.C["N_DIGITS"])
			for player in self.players:
				player.wallet += self.UD
	
	# End turn
	def endTurn(self):
		pass
