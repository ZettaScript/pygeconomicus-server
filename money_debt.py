#!/usr/bin/env python3

"""
	This file is part of Pygeconomicus-server.

	Pygeconomicus-server is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Pygeconomicus-server is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pygeconomicus-server.  If not, see <https://www.gnu.org/licenses/>.
"""

from utils import *

"""
	Not implemented yet.
	See money_free.py for a model.
"""

# Debt money
class MoneySystem():
	name = "debt_money"
	def __init__(self):
		pass
